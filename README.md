

This is a `node.js` component that triggers an export of the `rav` database for integration purposes. 


## Build
--------

It's a `node.js` application:

- written `typescript`,
- linted with `eslint`,
- transpiled by `esbuild`,
- into `commonjs` modules.

Managed with `yarn`, suppports:

- `yarn start`:  lints, typechecks, transpiles, and `serve`s the app, restarting it on changes to code or build scripts.
- `yarn ci`:  lints strictly, typechecks, transpiles, and runs the app.

Run `yarn start` in development and `yarn build` in CI.

Some details:

* watching realies on `nodemon`, with configuration in `nodemon.js`. 
* `nodemon` watches all build scripts  as well as the source code, including `nodemon.js` itself. 
* the linter configuration is in `.eslintrc.js`.
* the type checking configuration is in `tsconfig.json`.
* the `esbuild` configuration is in `esbuild.js`, and uses automatically settings from `tsconfig.json`. 
* the build is bundled, so that it can be started without installing dependencies. 
* the build includes links to external sourcemaps, limited to application sources (excludes bundlàùed dependencies).

## Debugging
-----------

`yarn start` opens a port (`9229`) for debugging.  In `VS Code`, this launch configuration in `.vscode/launch.json` attaches the debugger to the `node` process:

```json
{ 
    "projectName": "...",
    "name": "Attach Node",
    "request": "attach",
    "type": "node",
    "hostName": "localhost",
    "port": 9229
}
```

Alternatively, this launch configuration runs the app in the debugger (`F5`):

```json
{
        
    "projectName": "...",
    "name": "Debug Node",
    "request": "launch",
    "type": "node",
    "runtimeArgs": [ "build/index.js"]

```

## Externals
------------

Packages that cannot be bundled can be specifies as `dependencies` in `externals.json`. 

This is a minimal module manifest and can be installed to run the code with minimal dependencies.

## Resources
-------------

During development, mount test translations under `resources/locale`, and they will be copied into the build output.
The wlll not be under source control.


## Image
--------

There's a `Dockerfile`  to packages the application in a `Docker` image. In particular, it:

* copies `externals.json` as `package.json` under a root `app` folder.
* runs `yarn install` to install the external dependencies.
* copies the build output to the app folder.
* finally, lunches node with the transpiled code.

The image should be created after a build process, typically `yarn ci`. Dependency installation should be fast, as there should be very few externals and the image layers will be reused as long as there are no changes to `externals.json`. Running containers should also be snappy, as there are not further installation costs.


## Read-Only Authorization
--------------------------

To allow external users to access the export database with read-only grants we must run a `granting` script when the tables are created.
Ususally tables are created when the export routine runs for the first time, and this may occur on firts deployment or when tables are altered or created by new requirements of the exporter.

an example of granting is as follow:
```
GRANT USAGE ON SCHEMA rav TO rouser;
GRANT USAGE ON SCHEMA rav_public TO rouser;

GRANT SELECT ON
    rav.export_log,
    rav.record,
    rav.record_history,
    rav.record_history_delisted,
    rav.record_history_to_gear,
    rav.record_history_to_trx_authz,
    rav.record_to_gear,
    rav.record_to_trx_authz,
    rav_public.record_to_gear,
    rav_public.record,
    rav_public.record_history_to_gear,
    rav_public.record_history_delisted,
    rav_public.record_history
TO rouser;
```

Revoking permissions can be manually triggered with:
```
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA rav FROM rouser;
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA rav_public FROM rouser;
```

A full granting script can be found in the `rav-resources` project.




