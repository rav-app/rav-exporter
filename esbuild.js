
const fs = require('fs')

const { build } = require('esbuild')

const noVendorSourceMaps = {
  name: 'excludeVendorSourceMaps',
  setup(build) {
  build.onLoad({ filter: /node_modules/ }, args => {

    if (args.path.match(/\.(c|m)?js$/)) { 
      return {
        contents: fs.readFileSync(args.path, 'utf8')
          + '\n//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIiJdLCJtYXBwaW5ncyI6IkEifQ==',
        loader: 'default',
      }
    }
    })
  },
}


require('read-tsconfig')().then(tsconfig => {

  build({
    target: tsconfig.compilerOptions.target,
    entryPoints: tsconfig.files,
    outdir: tsconfig.compilerOptions.outDir,
    tsconfig: "tsconfig.json",
    bundle: true,
    sourcemap: true,
    external: Object.keys(require("./externals.json").dependencies),
    format: "cjs",
    platform: "node",
    plugins: [noVendorSourceMaps]
  })


})
  .catch(() => process.exit(1))
