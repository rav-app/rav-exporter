
export type Tag = {

    id: string
    code: string
    name?: Partial<{
        en: string
        fr: string
    }>
    description?: Partial<{
        en: string
        fr: string
    }>
    category: string

}

export type Tenant = {
    id: string
    code: string
    name: {
        en: string
        fr: string
    }
}

export type Contact = {
    id: string
    type: string
    name?: string
    address?: string
    regno?: string
}

export type RavPhoto = {

    type: string
    ref: {
        id: string
    }

}

export type RavTrxAuth = {
    id: string
    timestamp: string
    authzs: Record<string, {
        to: string
        from: string
        type: string
        timestamp: string
    }[]>
    //record_id?: string
}


export type RavRecord = {
    id: string
    tenant: string
    timestamp: string
    submissionTimestamp: string
    uvi: string

    patchedSlots?: string[]

    lifecycle: {
        created: number
        lastModified: number
    }

    delisting?: {
        date: string
        id: string
        note: string
        reason: string
        timestamp: string
    }

    authorization?: {
        id: string
        timestamp: string
        from: string
        to: string
    }

    details?: Partial<{

        id: string
        timestamp: string
        state: string
        flagstate: string
        range: string
        name: string
        vesselType: string
        vesselKind: string

        contacts: Contact[]


        gears: string[]
        identifiers: string[]

        specifications: Record<string, any>

        patchedSlots: 'details' | 'authorization' | 'photograph'[]

        port: {
            code: string,
            flag: string
            name: string
        }


    }>

    trxauthz?: RavTrxAuth

    photograph?: {
        id: string
        photos: RavPhoto[]
    }

}

export type ExportRavRecord = {

    id: string
    authorized_from: string | undefined
    authorized_to: string | undefined
    cc_tons: number | undefined
    ccm3: number
    date_reported: string
    date_last_changed: string
    flag_country: string
    flag_country_name: string
    grt: number | undefined
    imo: string | undefined
    ircs: string | undefined
    loa: number | undefined
    operating_range: string
    operator_name: string | undefined
    owner_name: string | undefined
    port_of_registration: string | undefined
    registration_no: string | undefined
    type_record: string
    vessel_name: string | undefined
    iotc_no: string
    vessel_type: string
    operator_address: string | undefined
    owner_address: string | undefined
    beneficiary_owner_address: string | undefined
    beneficiary_owner_name: string | undefined
    photo_starboard: string | undefined
    photo_portside: string | undefined
    photo_bow: string | undefined

}

export type ExportLog = {

    user: string
    status: string
    start: Date
    end: Date
    mode: string
    record_count: number
    record_history_count: number

}

export type ExportEvent = {

    start: string,
    user?: string,

}

export type CompletionEvent =  ExportLog

export type FailureEvent = ExportEvent & {

    error: string
}

export type ExportMode = 'all' | 'current'

export type AppSettings = {
    system: {
        excludedFlags: string[]
    }
}

export const defaultExclusionList = [] as string[]