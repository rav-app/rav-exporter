import { NatsConnection } from 'nats'
import { Settings } from './admin'
import { ExportLog } from './model'
import { DatabasePools } from './pools'

type State = {

    settings: Settings

    bus: NatsConnection

    pools: DatabasePools

    lastExport: ExportLog
    
}


const data =  {

    settings: { exportEnabled: false }

    
} as State

export const state = () => data

export const updateLastExport = async () => {
    
    const lastExport = await state().pools.getExportStatus()

    state().lastExport = lastExport ?? {} as ExportLog

}