import { ClientConfig } from 'pg'


export const defaultPort = '9999'

export  const port = process.env.PORT ?? defaultPort


export const exportApi="/"
export const exportApiStatus="/status"


export const INSERT_CHUNK = process.env.INSERT_CHUNK ? Number(process.env.INSERT_CHUNK) : 100

export const DEFAULT_DATE_FORMAT = process.env.DATE_FORMAT ?? 'dd/MM/yyyy'

export const DEBUG = process.env.DEBUG ? process.env.DEBUG === 'true' ? true : false : false

export const SYSTEM_USER = 'SYSTEM'

export const BYTESTREAM_URL = process.env.BYTESTREAM_PUBLIC_URL ?? 'https://localhost:3000/domain/stream/'


// reads configuration from environment, defaulting to local source and target.

export const db_host = process.env.DB_HOST ?? 'localhost'
export const db_port = process.env.DB_PORT ?? '5432'
export const db_name = process.env.DB_NAME ?? 'apprise'
export const db_user = process.env.DB_USER ?? 'apprise'
export const db_pwd = process.env.DB_PWD ?? 'apprise'
export const db_ssl = process.env.DB_SSL ?? "false"
export const db_schema = process.env.DB_SCHEMA ?? 'rav'

export const exportdb_host = process.env.EXPORTDB_HOST ?? db_host
export const exportdb_port = process.env.EXPORTDB_PORT ?? db_port
export const exportdb_name = process.env.EXPORTDB_NAME ?? 'rav_export'
export const exportdb_user = process.env.EXPORTDB_USER ?? db_user
export const exportdb_pwd = process.env.EXPORTDB_PWD ?? db_pwd
export const exportdb_schema = process.env.EXPORTDB_SCHEMA ?? db_schema
export const exportdb_public_schema = process.env.EXPORTDB_PUBLIC_SCHEMA ?? db_schema
export const exportdb_ssl = process.env.EXPORTDB_SSL ?? "false"


export const dbconfig: ClientConfig = {
    host: db_host,
    port: parseInt(db_port),
    database: db_name,
    user: db_user,
    password: db_pwd,
    ssl: db_ssl.toLowerCase() === 'true' ? {rejectUnauthorized: false} : false
} 

export const exportdbconfig = {
    host: exportdb_host,
    port: parseInt(exportdb_port),
    database: exportdb_name,
    user: exportdb_user,
    password: exportdb_pwd,
    ssl: db_ssl.toLowerCase() === 'true' ? {rejectUnauthorized: false} : false
} 

export const settings_endpoint = process.env.SETTINGS_ENDPOINT
export const bus_host= process.env.BUS_HOST
export const settings_topic = 'settings'

export const APP_SETTINGS_ID = 'BS-settings'

export const export_complete_topic = 'export_success'
export const export_failure_topic = 'export_failure'
export const cache_refresh_topic = 'cache_refresh'