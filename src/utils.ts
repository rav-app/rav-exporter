import { performance } from "perf_hooks"

export const index = <T>(values: T[] = []) => ({

    mappingBy: <S>(key: (_: T) => string | undefined, value: (_: T) => S) => {

      const out: { [_: string]: S } = {}

      for (let i = 0; i < values.length; i++) {

        const next = values[i]
        const nextkey = key(next)

        if (nextkey)
          out[nextkey] = value(next)

      }

      return out
    }
    ,

    by: (key: (_: T) => string | undefined) => index(values).mappingBy(key, t => t),

    byGroup: (key: (_: T) => string | undefined) => index(values).mappingByGroup(key, t => t),

    mappingByGroup: <S>(key: (_: T) => string | undefined, value: (_: T) => S) => {

      const out: { [_: string]: S[] } = {}

      for (let i = 0; i < values.length; i++) {

        const next = values[i]
        const nextkey = key(next)
        
        if (nextkey){
          const curr = out[nextkey]
          if (curr)
             curr.push(value(next))
          else
            out[nextkey] = [value(next)]
        }
      }

      return out
    }      

  })


  export const split = <T>(values: T[]) => {
    const self = {

      in: (chunk: number) => Object.values(

        values.reduce((acc, next, i) =>

          ({ ...acc, [Math.floor(i / chunk)]: [...acc[Math.floor(i / chunk)] ?? [], next] })


          , {} as Record<number, T[]>))

      ,

      max: (maxchunk: number) => {
        let chunk = maxchunk

        Array.from({ length: maxchunk + 1 }).map((_, i) => i).filter(i => i > 1).reduce((a, c) => {
          const reminder = values.length % c

          if (a >= reminder) {
            chunk = c
            return reminder
          }
          return a
        }, maxchunk)
        return self.in(chunk)
      }
    }
    return self
  }

  export const wait = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

  export const clock = async <T>(name: string, f: () => Promise<T>, logicalTasks?: number | ((_:T) => number)) => {

    const t0 = performance.now()
    const result = await f()
    const t1 = performance.now() - t0
  
    const tasks = typeof logicalTasks === 'number' ? logicalTasks : logicalTasks?.(result)
  
    console.log(`${name} took ${t1} ms. ${tasks ? `(${Math.round(tasks*1000/t1)}/sec.)`:''}`)
  
    return result;
  
  }