import { connect, JSONCodec } from 'nats'
import { bus_host } from './constants'
import { state } from './state'

export const codec = JSONCodec()


export const initBusConnection = async () => {

    try {

        const bus = await connect({ servers: bus_host })

        bus.closed().then(err => {
            if (err)
                console.log(`error disconnecting from event bus`, err)
        })

        console.log(`connected to event bus (${bus.getServer()})...`);

        state().bus = bus

    }
    catch (e) {

        console.error("can't subscribe to event bus.", e)
        throw e

    }
}