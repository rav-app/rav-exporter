import { cache_refresh_topic, export_complete_topic, export_failure_topic } from './constants'
import { codec } from './events'
import { exportTask } from './export'
import { CompletionEvent, ExportMode, FailureEvent } from './model'
import { state, updateLastExport } from './state'


// runs a task to export data and fire events based on outcome.

export const exportService = (mode: ExportMode) => {

    const self = {

        runFor: async (user: string | undefined) => {

            const { bus, pools } = state()

            const start = new Date().toISOString();

            const baseEvent = { start, user }

            console.log("\nexport task started -----------------------------------------------------------------\n")


            try {

                const task = exportTask(mode)

                await pools.runTask(task.runFor(user))

                await updateLastExport()

                const event: CompletionEvent = state().lastExport

                console.log(`\nfiring success event`, event)

                bus.publish(export_complete_topic, codec.encode(event))

                bus.publish(cache_refresh_topic, codec.encode({}))


            }
            catch (err) {

                const event: FailureEvent = { ...baseEvent, error: `${err}` }

                console.trace("\nfiring failure event", event)

                bus.publish(export_failure_topic, codec.encode(event))


            }
            finally {

                console.log("\nexport task completed -----------------------------------------------------------------")
            }

        }
    }

    return self

}

