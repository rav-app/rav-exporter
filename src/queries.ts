import { APP_SETTINGS_ID } from "./constants"

export const SOURCE_TENANT_TABLE = 'tenant'
export const SOURCE_TAG_TABLE = 'tag'
export const SOURCE_RECORD_TABLE = 'record'
export const SOURCE_CURRENTRECORD_TABLE = 'currentrecord'
export const SOURCE_BYTESTREAM_CONTENTS_TABLE = 'bytestream_content'

export const TARGET_RECORD_TABLE = 'record'
export const TARGET_RECORD_HISTORY_TABLE = 'record_history'
export const TARGET_RECORD_TO_GEARS_TABLE = `${TARGET_RECORD_TABLE}_to_gear`
export const TARGET_RECORD_TO_GEARS_HISTORY_TABLE = `${TARGET_RECORD_HISTORY_TABLE}_to_gear`
export const TARGET_RECORD_TO_TRX_AUTHZ_TABLE = `${TARGET_RECORD_TABLE}_to_trx_authz`
export const TARGET_RECORD_TO_TRX_AUTHZ_HISTORY_TABLE = `${TARGET_RECORD_HISTORY_TABLE}_to_trx_authz`
// export const TARGET_RECORD_DELISTED_TABLE = `${TARGET_RECORD_TABLE}_delisted`
export const TARGET_RECORD_DELISTED_HISTORY_TABLE = `${TARGET_RECORD_HISTORY_TABLE}_delisted`

export const TARGET_LOG_TABLE = 'export_log'
export const LOG_RECORDS_COUNT_COLUMN = `${TARGET_RECORD_TABLE}_count`
export const LOG_RECORDS_HISTORY_COUNT_COLUMN = `${TARGET_RECORD_HISTORY_TABLE}_count`
export const LOG_RECORD_TO_GEAR_COUNT_COLUMN = `${TARGET_RECORD_TO_GEARS_TABLE}_count`


export const sourceQueries = (schema: string) => ({

    GET_ALL_TAGS: `SELECT details FROM ${schema}.${SOURCE_TAG_TABLE}`,
    GET_ALL_RECORDS: `SELECT details FROM ${schema}.${SOURCE_RECORD_TABLE}`,
    GET_ALL_CURRENTRECORDS: `SELECT details FROM ${schema}.${SOURCE_CURRENTRECORD_TABLE}`,
    GET_ALL_TENANTS: `SELECT details FROM ${schema}.${SOURCE_TENANT_TABLE}`,
    GET_APP_SETTINGS: `SELECT bytes FROM ${schema}.${SOURCE_BYTESTREAM_CONTENTS_TABLE} WHERE stream = '${APP_SETTINGS_ID}'`

})


export const targetQueries = (schema: string, publicSchema: string) => ({

    TRUNCATE_RECORD_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_TABLE} CASCADE`,
    TRUNCATE_RECORD_HISTORY_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_HISTORY_TABLE} CASCADE`,
    TRUNCATE_RECORD_TO_GEARS_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_TO_GEARS_TABLE} CASCADE`,
    TRUNCATE_RECORD_HISTORY_TO_GEARS_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE} CASCADE`,
    TRUNCATE_RECORD_TXR_AUTHZ_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_TO_TRX_AUTHZ_TABLE} CASCADE`,
    TRUNCATE_RECORD_HISTORY_TXR_AUTHZ_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_TO_TRX_AUTHZ_HISTORY_TABLE} CASCADE`,
    // TRUNCATE_RECORD_DELISTED_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_DELISTED_TABLE} CASCADE`,
    TRUNCATE_RECORD_HISTORY_DELISTED_TABLE: `TRUNCATE ${schema}.${TARGET_RECORD_DELISTED_HISTORY_TABLE} CASCADE`,

    LOG_EXECUTION: `INSERT INTO ${schema}.${TARGET_LOG_TABLE} VALUES ($1::text, $2::text, $3, $4, $5::text, $6::numeric, $7::numeric)`,
    LOG_EXECUTION_ERROR: `INSERT INTO ${schema}.${TARGET_LOG_TABLE} VALUES ($1::text, $2::text, $3, $4)`,
    GET_LAST_SUCCESS_LOG: `SELECT * FROM ${schema}.${TARGET_LOG_TABLE} WHERE status = 'SUCCESS' ORDER BY ended DESC LIMIT 1`,

    STAGE_TABLES: `CREATE SCHEMA IF NOT EXISTS ${schema};
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_TABLE}
    (
        id                          text,
        iotc_no                     text,
        confidential                boolean,
        vessel_name                 text,
        vessel_type                 text,
        flag_country                text,
        flag_country_name           text,
        authorized_from             timestamp,
        authorized_to               timestamp,
        timestamp                   timestamp,
        submission_timestamp        timestamp,
        cc_tons                     numeric,
        ccm3                        numeric,
        grt                         numeric,
        gt                          numeric,
        imo                         text,
        ircs                        text,
        loa                         numeric,
        operating_range             text,
        port_of_registration        text,
        registration_no             text,
        type_record                 text,
        operator_name               text,
        operator_address            text,
        owner_name                  text,
        owner_address               text,
        beneficiary_owner_name      text,
        beneficiary_owner_address   text,
        operating_company_name      text,
        operating_company_address   text,
        operating_company_regno     text,
        photo_starboard             text,
        photo_portside              text,
        photo_bow                   text,
        PRIMARY KEY(id)
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_TO_GEARS_TABLE}
    (
        record_id                  text,
        gear_id                    text,
        CONSTRAINT fk_op_id FOREIGN KEY(record_id) REFERENCES ${schema}.${TARGET_RECORD_TABLE}(id)
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_TO_TRX_AUTHZ_TABLE}
    (
        record_id                  text,
        country_code               text,
        country                    text,
        type                       text,
        date_from                  timestamp,
        date_to                    timestamp,
        CONSTRAINT fk_op_history_delisted_id FOREIGN KEY(record_id) REFERENCES ${schema}.${TARGET_RECORD_TABLE}(id)
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_LOG_TABLE}
    (
        userid                              text default 'SYSTEM',
        status                              text,
        started                             timestamp,
        ended                               timestamp,
        mode                                text,
        ${LOG_RECORDS_COUNT_COLUMN}         integer default 0,
        ${LOG_RECORDS_HISTORY_COUNT_COLUMN} integer default 0
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_HISTORY_TABLE} (
        id                          text,
        iotc_no                     text,
        confidential                boolean,
        vessel_name                 text,
        vessel_type                 text,
        flag_country                text,
        flag_country_name           text,
        authorized_from             timestamp,
        authorized_to               timestamp,
        timestamp                   timestamp,
        submission_timestamp        timestamp,
        cc_tons                     numeric,
        ccm3                        numeric,
        grt                         numeric,
        gt                          numeric,
        imo                         text,
        ircs                        text,
        loa                         numeric,
        operating_range             text,
        port_of_registration        text,
        registration_no             text,
        type_record                 text,
        operator_name               text,
        operator_address            text,
        owner_name                  text,
        owner_address               text,
        beneficiary_owner_name      text,
        beneficiary_owner_address   text,
        operating_company_name      text,
        operating_company_address   text,
        operating_company_regno     text,
        photo_starboard             text,
        photo_portside              text,
        photo_bow                   text,
        PRIMARY KEY(id)
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE}
    (
        record_id                  text,
        gear_id                    text,
        CONSTRAINT fk_op_history_id FOREIGN KEY(record_id) REFERENCES ${schema}.${TARGET_RECORD_HISTORY_TABLE}(id)
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_DELISTED_HISTORY_TABLE}
    (
        record_id                  text,
        reason                     text,
        note                       text,
        CONSTRAINT fk_op_history_delisted_id FOREIGN KEY(record_id) REFERENCES ${schema}.${TARGET_RECORD_HISTORY_TABLE}(id)
    );
    CREATE TABLE IF NOT EXISTS ${schema}.${TARGET_RECORD_TO_TRX_AUTHZ_HISTORY_TABLE}
    (
        record_id                  text,
        country_code               text,
        country                    text,
        type                       text,
        date_from                  timestamp,
        date_to                    timestamp,
        CONSTRAINT fk_op_history_delisted_id FOREIGN KEY(record_id) REFERENCES ${schema}.${TARGET_RECORD_HISTORY_TABLE}(id)
    );
    `

    ,

    STAGE_PUBLIC_VIEWS: (excluded: string[]) => {

        const excludedSQL = excluded.length > 0 ? `WHERE flag_country NOT IN (${excluded.map(e => `'${e}'`).join(',')})` : ''

        return `CREATE SCHEMA IF NOT EXISTS ${publicSchema};
        CREATE OR REPLACE VIEW ${publicSchema}.${TARGET_RECORD_TABLE} AS SELECT * FROM ${schema}.${TARGET_RECORD_TABLE} ${excludedSQL};
        CREATE OR REPLACE VIEW ${publicSchema}.${TARGET_RECORD_HISTORY_TABLE} AS SELECT * FROM ${schema}.${TARGET_RECORD_HISTORY_TABLE} ${excludedSQL};
        CREATE OR REPLACE VIEW ${publicSchema}.${TARGET_RECORD_TO_GEARS_TABLE} AS SELECT rtg.record_id, rtg.gear_id FROM ${schema}.${TARGET_RECORD_TABLE} r JOIN ${schema}.${TARGET_RECORD_TO_GEARS_TABLE} rtg ON r.id = rtg.record_id ${excludedSQL};
        CREATE OR REPLACE VIEW ${publicSchema}.${TARGET_RECORD_DELISTED_HISTORY_TABLE} AS SELECT rtg.record_id, rtg.reason, rtg.note FROM ${schema}.${TARGET_RECORD_HISTORY_TABLE} r JOIN ${schema}.${TARGET_RECORD_DELISTED_HISTORY_TABLE} rtg ON r.id = rtg.record_id ${excludedSQL};
        CREATE OR REPLACE VIEW ${publicSchema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE} AS SELECT rtg.record_id, rtg.gear_id FROM ${schema}.${TARGET_RECORD_HISTORY_TABLE} r JOIN ${schema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE} rtg ON r.id = rtg.record_id ${excludedSQL};
        `

    }

    ,

    CREATE_INDEXES: `CREATE INDEX IF NOT EXISTS iotc_no_idx ON ${schema}.${TARGET_RECORD_TABLE} (iotc_no);
    CREATE INDEX IF NOT EXISTS flag_idx ON ${schema}.${TARGET_RECORD_TABLE} (flag_country);
    CREATE INDEX IF NOT EXISTS iotc_no_flag_idx ON ${schema}.${TARGET_RECORD_TABLE} (iotc_no, flag_country);
    CREATE INDEX IF NOT EXISTS vessel_type_idx ON ${schema}.${TARGET_RECORD_TABLE} (vessel_type);
    CREATE INDEX IF NOT EXISTS authorized_from_idx ON ${schema}.${TARGET_RECORD_TABLE} (authorized_from);
    CREATE INDEX IF NOT EXISTS authorized_to_idx ON ${schema}.${TARGET_RECORD_TABLE} (authorized_from, authorized_to);
    CREATE INDEX IF NOT EXISTS authorized_from_authorized_to_idx ON ${schema}.${TARGET_RECORD_TABLE} (authorized_to);
    CREATE INDEX IF NOT EXISTS timestamp_idx ON ${schema}.${TARGET_RECORD_TABLE} (timestamp);
    CREATE INDEX IF NOT EXISTS submission_timestamp_idx ON ${schema}.${TARGET_RECORD_TABLE} (submission_timestamp);
    CREATE INDEX IF NOT EXISTS iotc_no_timestamp_idx ON ${schema}.${TARGET_RECORD_TABLE} (iotc_no, timestamp);
    CREATE INDEX IF NOT EXISTS iotc_no_submission_timestamp_idx ON ${schema}.${TARGET_RECORD_TABLE} (iotc_no, submission_timestamp);
    CREATE INDEX IF NOT EXISTS iotc_no_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (iotc_no);
    CREATE INDEX IF NOT EXISTS flag_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (flag_country);
    CREATE INDEX IF NOT EXISTS iotc_no_flag_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (iotc_no, flag_country);
    CREATE INDEX IF NOT EXISTS vessel_type_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (vessel_type);
    CREATE INDEX IF NOT EXISTS authorized_from_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (authorized_from);
    CREATE INDEX IF NOT EXISTS authorized_to_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (authorized_from, authorized_to);
    CREATE INDEX IF NOT EXISTS authorized_from_authorized_to_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (authorized_to);
    CREATE INDEX IF NOT EXISTS timestamp_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (timestamp);
    CREATE INDEX IF NOT EXISTS submission_timestamp_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (submission_timestamp);
    CREATE INDEX IF NOT EXISTS iotc_no_timestamp_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (iotc_no, timestamp);
    CREATE INDEX IF NOT EXISTS iotc_no_submission_timestamp_history_idx ON ${schema}.${TARGET_RECORD_HISTORY_TABLE} (iotc_no, submission_timestamp);
    CREATE INDEX IF NOT EXISTS export_log_ended ON ${schema}.${TARGET_LOG_TABLE} (ended);
    CREATE INDEX IF NOT EXISTS export_log_status ON ${schema}.${TARGET_LOG_TABLE} (status);
    CREATE INDEX IF NOT EXISTS export_log_status_ended ON ${schema}.${TARGET_LOG_TABLE} (status,ended);
    CREATE INDEX IF NOT EXISTS rec_history_delisted_rec_id_idx ON ${schema}.${TARGET_RECORD_DELISTED_HISTORY_TABLE} USING BTREE (record_id);
    CREATE INDEX IF NOT EXISTS rec_history_to_gear_rec_id_idx ON ${schema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE} USING BTREE (record_id);
    CREATE INDEX IF NOT EXISTS rec_history_to_gear_gear_id_idx ON ${schema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE} USING BTREE (gear_id);
    CREATE INDEX IF NOT EXISTS rec_history_to_gear_rec_gear_id_idx ON ${schema}.${TARGET_RECORD_TO_GEARS_HISTORY_TABLE} USING BTREE (record_id, gear_id);
    CREATE INDEX IF NOT EXISTS rec_to_gear_rec_id_idx ON ${schema}.${TARGET_RECORD_TO_GEARS_TABLE} USING BTREE (record_id);
    CREATE INDEX IF NOT EXISTS rec_to_gear_gear_id_idx ON ${schema}.${TARGET_RECORD_TO_GEARS_TABLE} USING BTREE (gear_id);
    CREATE INDEX IF NOT EXISTS rec_to_gear_rec_gear_id_idx ON ${schema}.${TARGET_RECORD_TO_GEARS_TABLE} USING BTREE (record_id, gear_id);
    CREATE INDEX IF NOT EXISTS rec_history_trx_authz_rec_id_idx ON ${schema}.${TARGET_RECORD_TO_TRX_AUTHZ_HISTORY_TABLE} USING BTREE (record_id);
    CREATE INDEX IF NOT EXISTS rec_trx_authz_rec_id_idx ON ${schema}.${TARGET_RECORD_TO_TRX_AUTHZ_TABLE} USING BTREE (record_id);
    `

    ,

    DROP_RECORD_INDEXES: `DROP INDEX IF EXISTS ${schema}.iotc_no_idx;
    DROP INDEX IF EXISTS ${schema}.flag_idx;
    DROP INDEX IF EXISTS ${schema}.iotc_no_flag_idx;
    DROP INDEX IF EXISTS ${schema}.vessel_type_idx;
    DROP INDEX IF EXISTS ${schema}.authorized_from_idx;
    DROP INDEX IF EXISTS ${schema}.authorized_to_idx;
    DROP INDEX IF EXISTS ${schema}.authorized_from_authorized_to_idx;
    DROP INDEX IF EXISTS ${schema}.timestamp_idx;
    DROP INDEX IF EXISTS ${schema}.submission_timestamp_idx;
    DROP INDEX IF EXISTS ${schema}.iotc_no_timestamp_idx;
    DROP INDEX IF EXISTS ${schema}.iotc_no_submission_timestamp_idx;
    DROP INDEX IF EXISTS ${schema}.rec_history_delisted_rec_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_history_to_gear_rec_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_history_to_gear_gear_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_history_to_gear_rec_gear_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_history_trx_authz_rec_id_idx;
    `
    ,

    DROP_RECORD_HISTORY_INDEXES: `DROP INDEX IF EXISTS ${schema}.iotc_no_history_idx;
    DROP INDEX IF EXISTS ${schema}.flag_history_idx;
    DROP INDEX IF EXISTS ${schema}.iotc_no_flag_history_idx;
    DROP INDEX IF EXISTS ${schema}.vessel_type_history_idx;
    DROP INDEX IF EXISTS ${schema}.authorized_from_history_idx;
    DROP INDEX IF EXISTS ${schema}.authorized_to_history_idx;
    DROP INDEX IF EXISTS ${schema}.authorized_from_authorized_to_history_idx;
    DROP INDEX IF EXISTS ${schema}.timestamp_history_idx;
    DROP INDEX IF EXISTS ${schema}.submission_timestamp_history_idx;
    DROP INDEX IF EXISTS ${schema}.iotc_no_timestamp_history_idx;
    DROP INDEX IF EXISTS ${schema}.iotc_no_submission_timestamp_history_idx;
    DROP INDEX IF EXISTS ${schema}.rec_to_gear_rec_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_to_gear_gear_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_to_gear_rec_gear_id_idx;
    DROP INDEX IF EXISTS ${schema}.rec_trx_authz_rec_id_idx;
    `

})
