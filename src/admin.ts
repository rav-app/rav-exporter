import fetch from 'node-fetch'
import { bus_host, settings_endpoint, settings_topic } from './constants'
import { codec } from './events'
import { state } from './state'


export type Settings = {

    exportEnabled: boolean
    excludedFlags?: string[]
    
}


export type ExternalSettings = Record<string, any> & { operation: Record<string, any> & Settings}



export const intern = (settings: ExternalSettings): Settings => {

    const { system } = settings

    const interned = { exportEnabled: !!system?.exportEnabled, excludedFlags: system?.excludedFlags ?? [] }
    
    console.log({external: settings, internal: interned})

    return interned

}


export const initAdminIntegration = async () => {

    if (!settings_endpoint) {
        console.error("can't fetch settings, no endpoint configured.")
        return
    }

    const settings = await fetch(settings_endpoint).then(res => res.json() as Promise<ExternalSettings>).then(intern)

    state().settings = settings

    if (!bus_host)
        return

    subscribeForSettingsChange()



}


const subscribeForSettingsChange = () => {

    const bus = state().bus

    const subscription = bus.subscribe(settings_topic)

    if (!subscription)
        throw new Error("can't subscribe for settings change.")

    const waitOnMessage = async () => {

        for await (const m of subscription) {

            const { settings } = codec.decode(m.data) as { settings: ExternalSettings }

            try {
                console.log("received settings", settings)
                state().settings = intern(settings)
            }
            catch (e) {
                console.error("can't process settings change:", e)
            }
        }
    }

    waitOnMessage()


}