
import { Pool, PoolClient, PoolConfig } from 'pg';
import { INSERT_CHUNK, dbconfig, exportdb_public_schema, exportdb_schema, exportdbconfig } from './constants';
import { state } from './state';
import { split } from './utils';
import { targetQueries } from './queries';
import { ExportLog } from './model';


export const initDbPools = () => {

    state().pools = poolapi()

    return state().pools
}

export type DatabasePools = ReturnType<typeof poolapi>


const TARGET_QUERIES = targetQueries(exportdb_schema, exportdb_public_schema)

const poolapi = () => {

    const source = createPoolHelper(dbconfig)
    const target = createPoolHelper(exportdbconfig)

    const self = {


        warmPools: async () => {

            // we dont'
            await self.runTask(async (source, target) => {

                return Promise.all([
                    source.query('select version()'),
                    target.query('select version()')
                ])
            })
        }

        ,

        initDb: async () => {
            await self.runTask(async (_, target) => {

                return Promise.all([
                    target.query(TARGET_QUERIES.STAGE_TABLES),
                    target.query(TARGET_QUERIES.CREATE_INDEXES)
                ])
            })
        }

        ,

        isExportOngoing: () => {

            const { idleCount: idle, totalCount: pooled } = target

            const full = pooled === 1
            const busy = full && idle === 0

            return busy
        }

        ,

        getExportStatus: async (): Promise<ExportLog | undefined> => {

            const fetched = await self.runTask(async (_, target) => 
                target.query(TARGET_QUERIES.GET_LAST_SUCCESS_LOG)
            )

            if (fetched.rows.length < 1) return undefined

            const row = fetched.rows[0]

            return {
                user: row.userid,
                status: row.status,
                start: row.started,
                end: row.ended,
                mode: row.mode,
                record_count: row.record_count,
                record_history_count: row.record_history_count
            }

        }

        ,

        runTask: async <T>(task: (source: PoolClient, target: PoolClient) => Promise<T>) => {



            // console.log("acquiring connections...")

            const sourceClient = await source.connect()
            const targetClient = await target.connect()

            // console.log("begin transaction...")

            targetClient.query("BEGIN")

            try {

                const result = await task(sourceClient, targetClient)

                // console.log("commit transaction...")

                targetClient.query("COMMIT")

                return result


            }
            catch (e) {

                console.log("rollback transaction...")

                targetClient.query("ROLLBACK")

                throw e

            }

            finally {

                //console.log("releasing connection...")

                sourceClient.release()
                targetClient.release()

                //console.log("target after task", targetDb.stats())
            }


        }

        ,

        helperFor: (client: PoolClient) => {


            const self = {

                insert: async <T>(table: string, allrows: T[][]) => {

                    let error;

                    const count = await split(allrows).in(INSERT_CHUNK).map(rowChunk =>

                        rowChunk.map(row => {

                            const values = row.map(val => {

                                if (val === null) return 'NULL'
                                
                                switch (typeof val) {

                                    case 'undefined': return 'NULL'
                                    case 'boolean': return val ? 'true' : 'false'
                                    case 'number': return `${val}`
                                    case 'string': return `${client.escapeLiteral(val)}`
                                    default:
                                        throw Error(`'unmappable value ${val}`)

                                }
                            })

                            return `(${values.join(",")})`

                        })


                    ).reduce(async (acc, chunk) => {

                        const stmt = `INSERT INTO ${exportdb_schema}.${table} VALUES ${chunk.join(",")}`

                        try {

                            const result = await client.query(stmt)

                            return (await acc) + result.rowCount
                        }

                        catch (e) {

                            error = e
                            return 0

                        }
                    }, Promise.resolve(0))

                    if (error)
                        throw error

                    return count

                }

                ,

                executeScript: async (script: string) => {


                    const allQueries = script.replace(/(\r\n|\n|\r)/gm, " ") // remove newlines
                        .replace(/\s+/g, ' ') // excess white space
                        .split(";") // split into all statements
                        .map(Function.prototype.call, String.prototype.trim)
                        .filter(function (el) { return el.length != 0 })


                    return Promise.all(allQueries.map(sql => client.query(sql)))

                }

            }

            return self;
        }
    }


    return self

}


const createPoolHelper = (config: PoolConfig) => {

    const name = config.database ?? config.host ?? config.connectionString ?? 'anon db'

    return new Pool({

        ...config,
        max: 1,                            //  no more than one connection in use at the time (we'd rather not re-export in rapid sequnce).
        idleTimeoutMillis: 0               //  kept forever in principle  (our pool doesn't grow, why shrink it. )


        , connectionTimeoutMillis: 3000

        //, log: (...args) => console.log("[pool", ...args)

    })

        // .on('connect', () => console.log("created connection to", name))
        // .on('acquire', () => console.log("acquired connection to", name))
        // .on('release', () => console.log("released connection to", name))

        // we need to catch it to avoid it blows up the runtime because un-handled
        .on('error', err => console.log("auto-invalidated connection to", name, err))

}
