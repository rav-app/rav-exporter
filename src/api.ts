
import { FastifyInstance } from 'fastify';
import { SYSTEM_USER, exportApi, exportApiStatus } from './constants';
import { state } from './state';
import { exportService } from './service'
import { ExportMode } from './model';

type PostBody = Partial<{
  user: string
  mode?: ExportMode
}>

export const initApi = (server: FastifyInstance) => server


    .get(exportApi, async (_, reply) => {

        return reply.code(200).send("all systems active...")
    })

    .get(exportApiStatus, async (_, reply) => {

        const lastExport = state().lastExport ?? {}

        return reply.code(200).send(lastExport)
    })

    .post<any>(exportApi, async (request, reply) => {

        const body = (request.body ?? {user : SYSTEM_USER}) as PostBody

        const user = body.user

        // Mode forced to 'all' because 'current' records mode is broken.
        //    'current' records mode should update the current records (as it does) but is not updating the history
        //    making the two views inconsistent.
        // const mode = body.mode ?? 'all'
        const mode = 'all'

        const { settings, pools } = state()

        if (!settings.exportEnabled) {

            if (user)
                console.warn('ignoring trigger: export disabled in settings...')

            return
        }

        // we serialise requests: if one is ongoing, the other is aborted.
        // we use a single-connection pool for this and check it's been created (pull full) and working (zero idles)

    
        if (pools.isExportOngoing()) {

            reply.code(204)     // we don't want an error, can't find better to signal export is ongoing.
            return
        }

        try {

            const service = exportService(mode)

            // run export in async and returns immediately.
            service.runFor(user)
            
            reply.code(202)     // signal export has started.


        } catch (err) {

            reply.code(500).send(JSON.stringify({ status: err }))

        }
    })



