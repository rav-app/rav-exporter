
import { initAdminIntegration } from './admin'
import { initApi } from './api'
import { initBusConnection } from "./events"

import { bus_host, dbconfig, db_pwd, exportdbconfig, exportdb_pwd, settings_endpoint } from './constants'
import { initServer } from './server'
import { initDbPools } from './pools'
import { updateLastExport } from './state'


Promise.resolve().then(async () => {

    try {

        console.log("starting with configuration:", {

            source: { ...dbconfig, password: [...db_pwd].reduce((acc, next, i) => acc + (i < 3 ? next : '*'), '') }
            ,
            target: { ...exportdbconfig, password: [...exportdb_pwd].reduce((acc, next, i) => acc + (i < 3 ? next : '*'), '') }
            ,
            settings_endpoint
            ,
            bus_host
        })

        console.log("starting server...")
        const server = initServer()

        console.log("initialising api...")
        initApi(server)

        console.log("initialising db pools...")
        const pools = initDbPools()
 
        console.log("warming db pools...")
        await pools.warmPools()
 
        console.log("initialising database...")
        await pools.initDb()

        console.log("initialising event bus connection...")
        await initBusConnection()

        console.log("fetching and subscribing for settings...")
        await initAdminIntegration()

        console.log("initializing state...")
        await updateLastExport()
    }
    catch (err) {

        console.error("can't initialise exporter", err)
    }

})

// console.log({

//     source: { ...dbconfig, password: [...db_pwd].reduce((acc, next, i) => acc + (i < 3 ? next : '*'), '') }
//     ,
//     target: { ...exportdbconfig, password: [...exportdb_pwd].reduce((acc, next, i) => acc + (i < 3 ? next : '*'), '') }
//     ,
//     settings_endpoint
//     ,
//     bus_host
// })


// initServer().then(initApi).then(initDbConnection).then(initBusConnection).then(initAdminIntegration)


   