import { PoolClient, QueryConfig } from 'pg'
import { BYTESTREAM_URL, SYSTEM_USER, db_schema, exportdb_public_schema, exportdb_schema } from './constants'
import { Contact, ExportMode, RavRecord, RavTrxAuth, Tag, Tenant } from './model'
import { TARGET_RECORD_DELISTED_HISTORY_TABLE, TARGET_RECORD_HISTORY_TABLE, TARGET_RECORD_TABLE, TARGET_RECORD_TO_GEARS_HISTORY_TABLE, TARGET_RECORD_TO_GEARS_TABLE, TARGET_RECORD_TO_TRX_AUTHZ_HISTORY_TABLE, TARGET_RECORD_TO_TRX_AUTHZ_TABLE, sourceQueries, targetQueries } from './queries'
import { state } from './state'
import { clock, index } from './utils'

const SOURCE_QUERIES = sourceQueries(db_schema)
const TARGET_QUERIES = targetQueries(exportdb_schema, exportdb_public_schema)

const SO_CATEGORY = 'TGC-operation-objective'
export const SO_MGMT_PROP = 'shared-management'
const IND_CATEGORY = 'TGC-operation-indicator'
export const IND_METHOD_PROP = 'indicat-agg-method'
// runs the 

export const exportTask = (mode: ExportMode) => {

    const self = {

        runFor: (user: string | undefined) => async (source: PoolClient, target: PoolClient) => {

            const start = new Date().toISOString()

            const task = taskapi(source, target);

            try {

                // await new Promise(resolve => setTimeout(resolve, 3000))

                await clock(" -> staging tables...", task.stageTables)
                await clock(' -> fetching tags...', task.fetchTags)
                await clock(' -> fetching tenants...', task.fetchTenants)
                await clock(' -> fetching current records...', task.fetchCurrentRecords)
                mode === 'all' && await clock(' -> fetching all records...', task.fetchAllRecords)

                const statsCurrent = await task.externAndWriteOut('current')
                const statsAll = mode === 'all' ? await task.externAndWriteOut('all') : { records_history: undefined, gearsToRecord: undefined }

                const stats = { mode, records: statsCurrent['records'] ?? 0, records_history: statsAll['records_history'] ?? 0 }

                await clock(' -> staging public views', task.createPublicViews)

                console.log("\ncompleted with stats", stats)

                await task.logSuccess(user, start, stats)

            }
            catch (err) {

                await task.logError(user, start, err ? `${err}` : undefined)

                throw err

            }
        }
    }

    return self
}

const taskapi = (source: PoolClient, target: PoolClient) => {

    const { pools } = state()

    const targetHelper = pools.helperFor(target)

    let tags: Record<string, Tag> = {}
    let tenants: Record<string, Tenant> = {}
    let records: RavRecord[] = []
    let currentrecords: RavRecord[] = []

    const modeFor = (mode: ExportMode) => {
        const recordsToParse = mode === 'all' ? records : currentrecords
        const truncateRecordTable = mode === 'all' ? TARGET_QUERIES.TRUNCATE_RECORD_HISTORY_TABLE : TARGET_QUERIES.TRUNCATE_RECORD_TABLE
        const toInsertTable = mode === 'all' ? TARGET_RECORD_HISTORY_TABLE : TARGET_RECORD_TABLE
        const dropIndexes = mode === 'all' ? TARGET_QUERIES.DROP_RECORD_HISTORY_INDEXES : TARGET_QUERIES.DROP_RECORD_INDEXES
        const recordToGearsTable = mode === 'all' ? TARGET_RECORD_TO_GEARS_HISTORY_TABLE : TARGET_RECORD_TO_GEARS_TABLE
        const truncateRecordsToGearTable = mode === 'all' ? TARGET_QUERIES.TRUNCATE_RECORD_HISTORY_TO_GEARS_TABLE : TARGET_QUERIES.TRUNCATE_RECORD_TO_GEARS_TABLE
        const trxAuthzTable = mode === 'all' ? TARGET_RECORD_TO_TRX_AUTHZ_HISTORY_TABLE : TARGET_RECORD_TO_TRX_AUTHZ_TABLE
        const truncatetrxAuthzTable = mode === 'all' ? TARGET_QUERIES.TRUNCATE_RECORD_HISTORY_TXR_AUTHZ_TABLE : TARGET_QUERIES.TRUNCATE_RECORD_TXR_AUTHZ_TABLE
        const delistedTable = mode === 'all' ? TARGET_RECORD_DELISTED_HISTORY_TABLE : undefined
        const truncateDelistedTable = mode === 'all' ? TARGET_QUERIES.TRUNCATE_RECORD_HISTORY_DELISTED_TABLE : undefined

        return { recordsToParse, truncateRecordTable, toInsertTable, dropIndexes, recordToGearsTable, truncateRecordsToGearTable, delistedTable, truncateDelistedTable, trxAuthzTable, truncatetrxAuthzTable }
    }

    const self = {

        stageTables: () => targetHelper.executeScript(TARGET_QUERIES.STAGE_TABLES)

        ,

        fetchTags: async () => {

            const sourceTags = await source.query(SOURCE_QUERIES.GET_ALL_TAGS)

            const parsed = [
                ...sourceTags.rows.map(row => JSON.parse(row.details) as Tag),
            ]

            tags = index(parsed).by(t => t.id)

        }
        ,

        fetchTenants: async () => {

            const fetched = await source.query(SOURCE_QUERIES.GET_ALL_TENANTS)

            const parsed = fetched.rows.map(row => JSON.parse(row.details) as Tenant)

            tenants = index(parsed).by(t => t.id)

        }

        ,

        fetchAllRecords: async () => {

            const fetched = await source.query(SOURCE_QUERIES.GET_ALL_RECORDS)

            records = fetched.rows.map(op => op.details as RavRecord)
        }

        ,

        fetchCurrentRecords: async () => {

            const fetched = await source.query(SOURCE_QUERIES.GET_ALL_CURRENTRECORDS)

            // Filter delisted as we don't want them in the record table
            currentrecords = fetched.rows.map(op => op.details as RavRecord).filter(rec => rec.delisting === undefined)
        }

        ,

        tenantIdToCode : (id: string): string => tenants[id]?.code ?? undefined

        ,

        externAndWriteOut: async (mode: ExportMode) => {

            const { settings } = state()

            const { recordsToParse, truncateRecordTable, toInsertTable, dropIndexes, recordToGearsTable, truncateRecordsToGearTable, delistedTable, truncateDelistedTable, trxAuthzTable, truncatetrxAuthzTable } = modeFor(mode)

            const tagIdToCode = (id: string): string => tags[id]?.code ?? id
            const tagIdToName = (id: string): string => tags[id]?.name?.en ?? id
            const tenantIdToName = (id: string): string => tenants[id]?.name.en ?? undefined

            const contactName = (contacts: Contact[], type: string) => contacts.find(c => c.type === type)?.name
            const contactAddress = (contacts: Contact[], type: string) => contacts.find(c => c.type === type)?.address
            const contactRegNo = (contacts: Contact[], type: string) => contacts.find(c => c.type === type)?.regno

            const getIdentifier = (identifiers: string[], identifier: string) => {

                const id = identifiers.filter(id => id.toLowerCase().startsWith(identifier.toLowerCase()))
                return id.length > 0 ? id[0].replace(identifier, '') : undefined
            }

            const getPhotoFor = (record: RavRecord, type: string): string | undefined => {
                if (record.photograph) {
                    const photo = record.photograph.photos.filter(p => p.type === type)
                    if (photo.length > 0) return BYTESTREAM_URL + photo[0].ref.id
                }
                return undefined
            }

            const externedRecords = await clock(` -> externing ${mode} records...`, async () => recordsToParse.map(record => {
                return {
                    id: record.id,
                    authorized_from: record.authorization?.from ? new Date(record.authorization.from).toISOString() : undefined,
                    authorized_to: record.authorization?.to ? new Date(record.authorization.to).toISOString() : undefined,
                    timestamp: record.timestamp ? new Date(record.timestamp).toISOString() : undefined,
                    submission_timestamp: record.submissionTimestamp ? new Date(record.submissionTimestamp).toISOString() : undefined,
                    cc_tons: record.details?.specifications?.['TG-totvoltonnes-specification'] ?? undefined,
                    ccm3: record.details?.specifications?.['TG-totvol-specification'] ?? undefined,
                    // date_reported: new Date(record.lifecycle.created).toISOString(),
                    // date_last_changed: record.lifecycle.lastModified ? new Date(record.lifecycle.lastModified).toISOString() : new Date(record.lifecycle.created).toISOString(),
                    confidential: settings.excludedFlags?.includes(record.details?.flagstate ?? ''),
                    flag_country: self.tenantIdToCode(record.details?.flagstate ?? ''),
                    flag_country_name: tenantIdToName(record.details?.flagstate ?? ''),
                    grt: record.details?.specifications?.['TG-grt-specification'] ?? undefined,
                    gt: record.details?.specifications?.['TG-tonnage-specification'] ?? undefined,
                    imo: getIdentifier(record.details?.identifiers ?? [], 'TG-imo-scheme://'),
                    ircs: getIdentifier(record.details?.identifiers ?? [], 'TG-ircs-scheme://'),
                    loa: record.details?.specifications?.['TG-loa-specification'] ?? undefined,
                    operating_range: tagIdToCode(record.details?.range ?? ''),
                    operator_name: contactName(record.details?.contacts ?? [], 'TG-operatorcontact'),
                    owner_name: contactName(record.details?.contacts ?? [], 'TG-owner-contact'),
                    port_of_registration: record.details?.port?.name ?? record.details?.port?.code,
                    registration_no: getIdentifier(record.details?.identifiers ?? [], 'TG-regno-scheme://'),
                    type_record: tagIdToCode(record.details?.vesselKind ?? ''),
                    vessel_name: record.details?.name,
                    iotc_no: record.uvi.replace('TG-iotc-scheme://', ''),
                    vessel_type: tagIdToCode(record.details?.vesselType ?? ''),
                    operator_address: contactAddress(record.details?.contacts ?? [], 'TG-operatorcontact'),
                    owner_address: contactAddress(record.details?.contacts ?? [], 'TG-owner-contact'),
                    beneficiary_owner_address: contactAddress(record.details?.contacts ?? [], 'TG-beneficialowner-contact'),
                    beneficiary_owner_name: contactName(record.details?.contacts ?? [], 'TG-beneficialowner-contact'),
                    operating_company_name: contactName(record.details?.contacts ?? [], 'TG-operatingcompany-contact'),
                    operating_company_address: contactAddress(record.details?.contacts ?? [], 'TG-operatingcompany-contact'),
                    operating_company_regno: contactRegNo(record.details?.contacts ?? [], 'TG-operatingcompany-contact'),
                    photo_starboard: getPhotoFor(record, 'TGC-starboard-photo'),
                    photo_portside: getPhotoFor(record, 'TGC-portside-photo'),
                    photo_bow: getPhotoFor(record, 'TGC-bow-photo')
                }
            }))

            const externedGearsToRecord: [string, string][] = await clock(` -> externing ${mode} records to gears...`, async () => {

                // return recordsToParse.reduce((acc, record) => !record.details?.gears ? acc :
                //     [...acc, ...record.details.gears.map(gear => [record.id, tagIdToCode(gear)]) as [string, string][]]
                //     , [] as [string, string][])

                let externed = [] as [string, string][]

                recordsToParse.forEach(record => (record.details?.gears ?? []).forEach(gear => externed.push([record.id, tagIdToCode(gear)])))

                return externed

            })

            const externedDelistedRecord: [string, string, string][] = await clock(` -> externing ${mode} delisted records...`, async () => {
                let externed = [] as [string, string, string][]

                recordsToParse.filter(record => record.delisting !== undefined).forEach(record => externed.push([record.id, tagIdToName(record.delisting!.reason), record.delisting!.note]))

                return externed
            })

            const externedTrxRecords: [string, string, string, string, string, string][] = await clock(` -> externing ${mode} trxauthz records...`, async () => {
                let externed = [] as [string, string, string, string, string, string][]

                const trx = recordsToParse.filter(record => record.trxauthz !== undefined).map(record => ({...record.trxauthz, record_id: record.id})) as (RavTrxAuth & {record_id: string})[]
                
                trx.forEach(trx => {
                    Object.keys(trx.authzs).forEach(country => {
                        trx.authzs[country].forEach(trxRecord => {
                            externed.push([
                                trx.record_id ?? '',  
                                self.tenantIdToCode(country),
                                tenantIdToName(country), 
                                tagIdToCode(trxRecord.type), 
                                new Date(trxRecord.from).toISOString(), 
                                new Date(trxRecord.to).toISOString()])
                        })
                    })
                })
                

                return externed
            })

            await clock(` -> dropping ${mode} indices...`, () => targetHelper.executeScript(dropIndexes))

            const recs = await clock(` -> writing ${mode} records...`, async () => {
                await target.query(truncateRecordTable)
                return targetHelper.insert(toInsertTable, externedRecords.map(rec => [
                    rec.id,
                    rec.iotc_no,
                    rec.confidential,
                    rec.vessel_name,
                    rec.vessel_type,
                    rec.flag_country,
                    rec.flag_country_name,
                    rec.authorized_from,
                    rec.authorized_to,
                    rec.timestamp,
                    rec.submission_timestamp,
                    rec.cc_tons,
                    rec.ccm3,
                    rec.grt,
                    rec.gt,
                    rec.imo,
                    rec.ircs,
                    rec.loa,
                    rec.operating_range,
                    rec.port_of_registration,
                    rec.registration_no,
                    rec.type_record,
                    rec.operator_name,
                    rec.operator_address,
                    rec.owner_name,
                    rec.owner_address,
                    rec.beneficiary_owner_name,
                    rec.beneficiary_owner_address,
                    rec.operating_company_name,
                    rec.operating_company_address,
                    rec.operating_company_regno,
                    // rec.date_reported,
                    // rec.date_last_changed,
                    rec.photo_starboard,
                    rec.photo_portside,
                    rec.photo_bow
                ]))
            })

            await clock(` -> writing ${mode} gears to records...`, async () => {
                await target.query(truncateRecordsToGearTable)
                return targetHelper.insert(recordToGearsTable, externedGearsToRecord)
            })

            if (truncateDelistedTable && delistedTable) {
                await clock(` -> writing ${mode} delisted records...`, async () => {
                    await target.query(truncateDelistedTable)
                    return targetHelper.insert(delistedTable, externedDelistedRecord)
                })
            }

            await clock(` -> writing ${mode} trx authz records...`, async () => {
                await target.query(truncatetrxAuthzTable)
                return targetHelper.insert(trxAuthzTable, externedTrxRecords)
            })

            await clock(` -> re-building ${mode} indices...`, () =>

                target.query(TARGET_QUERIES.CREATE_INDEXES)

            )

            return mode === 'all' ? { records: 0, records_history: recs } : { records: recs, records_history: 0 }

        }


        ,

        createPublicViews: () => {
            const { settings } = state()

            return targetHelper.executeScript(TARGET_QUERIES.STAGE_PUBLIC_VIEWS((settings.excludedFlags ?? []).map(self.tenantIdToCode)))

        }

        ,


        logSuccess: async (user: string | undefined, start: string, counts: any) => {

            const userId = user ?? SYSTEM_USER

            const query: QueryConfig = {

                text: TARGET_QUERIES.LOG_EXECUTION,
                values: [userId, 'SUCCESS', start, new Date().toISOString(), ...Object.values(counts)]
            }

            await target.query(query)

        }

        ,

        logError: async (user: string | undefined, start: string, error?: string) => {

            try {
                const errorType = error ?? 'ERROR'

                const userId = user ?? SYSTEM_USER

                const query = {
                    text: TARGET_QUERIES.LOG_EXECUTION_ERROR,
                    values: [userId, errorType, start, new Date().toISOString()]

                } as QueryConfig

                await target.query(query)
            }
            catch (err) {

                console.log("couldn't log error", err)
            }


        }

    }

    return self

}











