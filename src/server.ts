

import traps from '@dnlup/fastify-traps'
import fastify from 'fastify'
import { port } from './constants'


export const initServer = () => {

    const server = fastify()

    server.register(traps)

    server.listen({port:parseInt(port), host: '0.0.0.0'}, (err, address) => {  // listens to all addresses in-container

        if (err) {
            server.log.error(err)
            process.exit(1)
          }

        console.log(`ready listening on ${address}...`)

    })

    return server
}